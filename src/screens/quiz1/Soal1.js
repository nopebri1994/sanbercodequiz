import React,{useState, useEffect} from 'react';
import {View,Text} from 'react-native';

const Soal1 = () => {
    const [name,setName] = useState('Jhon Doe');

    useEffect(()=> {
        console.log(name);
        setTimeout(()=>{
            setName('asep');
        },3000);
        return () => {
            console.log(name);
        }
     })

    return(
        <View>
            <Text>{name}</Text>
        </View>
    )

}

export default Soal1