import React, {useContext} from 'react';
import {View, Text, FlatList} from 'react-native';
import {RootContext}  from './RootContext'

const Con_soal2 = () => {

    return (
        <RootContext.Consumer>
            {({name})=>(
                <View>
                    <FlatList 
                        data={name}
                        renderItem={({item})=>
                        <View>
                                <Text>Nama Saya Adalah : {item.name}, Jabatan {item.position}</Text>
                                <Text>----------------------------------------------------------</Text>
                        </View>
                    }
                    keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            )}
        </RootContext.Consumer>
    )
}

export default Con_soal2;