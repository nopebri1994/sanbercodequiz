import React, { useState, createContext } from 'react'
import Cons from './con_soal2'
import {RootContext} from './RootContext';


const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider value={{
            name
        }} >
             
            <Cons />
        </RootContext.Provider>
    )
}

export default ContextAPI;
